# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2

DESCRIPTION="Mind mapping software for Gnome"
HOMEPAGE="http://www.gnome.org/~dscorgie/labyrinth.html"
SRC_URI="http://www.gnome.org/~dscorgie/files/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=">=dev-lang/python-2.4
	>=x11-libs/gtk+-2.8
	>=dev-python/pygtk-2.10
	>=dev-python/pygobject-2.10
	>=dev-python/gnome-python-2.12
	>=gnome-base/gnome-desktop-2.14
	dev-python/pycairo"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
	>=dev-util/intltool-0.35"

