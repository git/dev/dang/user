# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2

DESCRIPTION="A library for developing mobile applications with E-mail functionality"
HOMEPAGE="http://www.tinymail.org"
SRC_URI="http://www.fprintf.net/downloads/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="python telepathy vala"

RDEPEND=">=dev-libs/glib-2.8
	>=x11-libs/gtk+-2.8
	>=gnome-base/gnome-vfs-2
	>=gnome-base/libgnomeui-2
	gnome-base/gnome-keyring
	net-misc/networkmanager
	net-libs/xulrunner
	python? (
		dev-lang/python
		dev-python/pygobject
		dev-python/pygtk
	)
	telepathy? ( net-libs/telepathy-glib )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

pkg_config() {
	G2CONF="--enable-gnome
	--enable-uigtk
	$(use_enable telepathy tp)
	$(use_enable python python-bindings)
	$(use_enable vala vala-bindings)"
}
