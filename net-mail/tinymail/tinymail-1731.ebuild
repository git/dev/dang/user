# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2

DESCRIPTION="A library for developing mobile applications with E-mail functionality"
HOMEPAGE="http://www.tinymail.org"
SRC_URI="http://www.fprintf.net/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="xulrunner"

RDEPEND=">=dev-libs/glib-2.8
	>=x11-libs/gtk+-2.8
	>=gnome-base/gnome-vfs-2
	>=gnome-base/libgnomeui-2
	gnome-base/gnome-keyring
	net-misc/networkmanager
	xulrunner? ( net-libs/xulrunner )
	!xulrunner? ( www-client/mozilla-firefox )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

pkg_config() {
	G2CONF="--enable-gnome --enable-uigtk"
}
