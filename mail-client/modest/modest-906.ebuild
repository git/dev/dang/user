# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit gnome2

DESCRIPTION="A small e-mail program specifically targetting modest hardware"
HOMEPAGE="http://modest.garage.maemo.org/"
SRC_URI="http://www.fprintf.net/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=">=dev-libs/glib-2.8
	>=x11-libs/gtk+-2.10
	>=gnome-base/gconf-2
	>=gnome-base/gnome-vfs-2
	|| (
		=gnome-extra/gtkhtml-3.12*
		=gnome-extra/gtkhtml-3.10*
		)
	net-mail/tinymail
	net-misc/networkmanager"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

